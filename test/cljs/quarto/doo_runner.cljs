(ns quarto.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [quarto.core-test]))

(doo-tests 'quarto.core-test)
